#include <stdio.h>

int ch (int x);
int convert (int x);

int main (void)
{
	int x, oe, next;
	
	printf ("Enter a number.\n");
	scanf ("%d", &x);
	
	oe = ch(x);
	
	if (oe == 0)
		printf("%d is even.\n", x);
	else
		printf("%d is odd.\n", x);
		
	next = convert(x);
		
	if (oe == 0)
		printf("By adding 1 to %d we get %d which is odd.\n", x, next);
	else
		printf("By adding 1 to %d we get %d which is even.\n", x, next);
}

int ch (int x)
{
	int oe;
	
	if (x % 2 == 0)
		oe = 0;
	else
		oe = 1;
	
	return oe;
}

int convert (int x)
{
	int next;
	next = x + 1;
	
	return next;
}
