#include <stdio.h>

int *input(int n)
{
    static int a[10], i;
    
    printf("Enter elements of array.\n");
    
    for (i = 0; i < n; i++)
        scanf("%d",&a[i]);
        
    return a;
}

void exponent()
{
    int s[10], c[10];
    int n, i;
    
    printf("Enter size of array.\n");
    scanf("%d",&n);
    
    int *a = input(n);
    
    for (i = 0; i < n; i++)
    {
        s[i] = a[i] * a[i];
        c[i] = s[i] * a[i];
    }
    
    for (i = 0; i < n; i++)
    {
        printf("\nSquare of %d = %d.\n", a[i], s[i]);
        printf("Cube of %d = %d.\n", a[i], c[i]);
    }
}

int main(void)
{
    exponent();
}
