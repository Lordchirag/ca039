#include <stdio.h>
#include <stdlib.h>

struct student
{
    char name[30];
    int rollno;
    char section[2];
    char department[5];
    float fees;
    int result;
};

int greater(int x, int y);

void inputAndProcess()
{
    struct student s[2];
    int larger;
    
    printf("Enter details of first student.\n\n");
    
    printf("Enter name.\n");
    scanf("%s", s[0].name);
    fflush(stdin);
    printf("Enter rollno.\n");
    scanf("%d", &s[0].rollno);
    printf("Enter section.\n");
    scanf("%s", s[0].section);
    fflush(stdin);
    printf("Enter department.\n");
    scanf("%s", s[0].department);
    fflush(stdin);
    printf("Enter fees.\n");
    scanf("%f", &s[0].fees);
    printf("Enter result.\n");
    scanf("%d", &s[0].result);
    
    printf("\nEnter details of second student.\n\n");
    
    printf("Enter name.\n");
    scanf("%s", s[1].name);
    fflush(stdin);
    printf("Enter rollno.\n");
    scanf("%d", &s[1].rollno);
    printf("Enter section.\n");
    scanf("%s", s[1].section);
    fflush(stdin);
    printf("Enter department.\n");
    scanf("%s", s[1].department);
    fflush(stdin);
    printf("Enter fees.\n");
    scanf("%f", &s[1].fees);
    printf("Enter result.\n");
    scanf("%d", &s[1].result);
    
    larger = greater (s[0].result, s[1].result);
    
    printf("Student with higher result is : \n\n");
    
    if (larger == s[0].result)
    {
        printf("Name :\t\t%s\n", s[0].name);
        printf("Rollno :\t%d\n", s[0].rollno);
        printf("Section :\t%s\n", s[0].section);
        printf("Department :\t%s\n", s[0].department);
        printf("Fees :\t\t%f\n", s[0].fees);
        printf("Result :\t%d\n", s[0].result);
    }
    else
    {
        printf("Name :\t\t%s\n", s[1].name);
        printf("Rollno :\t%d\n", s[1].rollno);
        printf("Section :\t%s\n", s[1].section);
        printf("Department :\t%s\n", s[1].department);
        printf("Fees :\t\t%f\n", s[1].fees);
        printf("Result :\t%d\n", s[1].result);
    }
}

int greater(int x, int y)
{
    return (x > y) ? x : y;
}

int main(void)
{
    inputAndProcess();
}