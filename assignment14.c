#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct adapter
{
    char model[20];
    float cost;

}Adapter;

void addItem();
void listItem();
void searchItem();
float increaseInCost(float cost);
void updateItem();
void man();

int main(void)
{
    char cmd[8];

    while (strcmp(cmd, "exit") != 0)
    {
        printf("Enter the command to perform any of the following operations : \n");
        printf("1. Add new adapter.\n");
        printf("2. List out all adapters.\n");
        printf("3. Search adapter.\n");
        printf("4. Update adapter specifications.\n");
        printf("5. Clear console.\n");
        printf("6. Exit.\n\n");

        printf("Use 'man' to get a list of commands.\n\n>>>");
        scanf("%s", cmd);

        if (strcmp(cmd, "add") == 0)
        {
            addItem();
        }
        else if (strcmp(cmd, "ls") == 0)
        {
            listItem();
        }
        else if (strcmp(cmd, "search") == 0)
        {
            searchItem();
        }
        else if (strcmp(cmd, "update") == 0)
        {
            updateItem();
        }
        else if (strcmp(cmd, "man") == 0)
        {
            man();
        }
        else if (strcmp(cmd, "clear") == 0)
        {
            system("cls");
        }
        else if (strcmp(cmd, "exit") == 0)
        {
            break;
        }
        else
        {
            printf("Invalid command or operation.\n");
        }
    }

    system("cls");
    printf("Terminated process");
}

void addItem()
{
    FILE *f;
    Adapter a;

    if (f == NULL)
    {
        f = fopen("data.ojs", "wb+");
        printf("Created new data file.\n\n\n");
    }
    else
    {
        f = fopen("data.ojs", "ab+");
    }

    printf("Enter adapter model (Spaces are not allowed).\n>>>");
    scanf("%s", &a.model);
    fflush(stdin);

    printf("Enter cost of the adapter in USD (The price will be increased by 10%).\n>>>");
    scanf("%f", &a.cost);

    a.cost = increaseInCost(a.cost);

    fwrite(&a, sizeof(a), 1, f);

    printf("\nSuccessfully added model.\n\n");

    fclose(f);
}

void listItem()
{
    FILE *f;
    Adapter a;
    int i;

    if ((f = fopen("data.ojs", "rb")) == NULL)
        printf("\nNo products found.\n\n");
    else
    {
        printf("\n\n");

        for(i = 0; i < 40; i++)
            printf("-");

        printf("\n");

        printf("MODEL\t\t\tCOST\n");

        for(i = 0; i < 40; i++)
            printf("-");

        printf("\n\n");

        while (fread(&a, sizeof(a), 1, f) == 1)
        {
            printf("%s\t\t%f\n", a.model, a.cost);
        }

        for(i = 0; i < 40; i++)
            printf("-");

        printf("\n\n");
    }

    fclose(f);
}

void searchItem()
{
    FILE *f;
    Adapter a;
    char model[20];
    int i, flag = 1;

    if ((f = fopen("data.ojs", "rb")) == NULL)
    {
        printf("No products available.\n");
    }
    else
    {
        printf("Enter adapter model.\n>>>");
        scanf("%s", &model);

        while (fread(&a, sizeof(a), 1, f) == 1)
        {
            if (strcmp(a.model, model) == 0)
            {
                printf("\n\n");

                for(i = 0; i < 40; i++)
                    printf("-");

                printf("\n");

                printf("MODEL\t\t\tCOST\n");

                for(i = 0; i < 40; i++)
                    printf("-");

                printf("\n\n");

                while (fread(&a, sizeof(a), 1, f) == 1)
                {
                    printf("%s\t\t%f\n", a.model, a.cost);
                }

                for(i = 0; i < 40; i++)
                    printf("-");

                printf("\n\n");

                flag = 0;
            }
            else if (flag == 1)
                printf("Unknown or invalid model.");
        }
    }

    fclose(f);
}

float increaseInCost(float cost)
{
    float increasedCost = cost * 0.1;
    float totalCost = cost + increasedCost;

    return totalCost;
}

void updateItem()
{
    FILE *f;
    Adapter a;
    char model[20];
    int i, flag = 1;

    if ((f = fopen("data.ojs", "rb+")) == NULL)
    {
        printf("No products available.\n");
    }
    else
    {
        printf("Enter adapter model.\n>>>");
        scanf("%s", &model);

        while (fread(&a, sizeof(a), 1, f) == 1)
        {
            if (strcmp(a.model, model) == 0)
            {
                printf("Enter adapter model (Spaces are not allowed).\n>>>");
                scanf("%s", &a.model);
                fflush(stdin);

                printf("Enter cost of the adapter in USD (The price will be increased by 10%).\n>>>");
                scanf("%f", &a.cost);

                a.cost = increaseInCost(a.cost);

                fwrite(&a, sizeof(a), 1, f);

                printf("\nSuccessfully changed model.\n\n");

                fclose(f);
                flag = 0;
            }
            else if (flag == 1)
                printf("Unknown or invalid model.");
        }
    }
}

void man()
{
    printf("\nUse 'add' to add new adapter.\n");
    printf("Use 'ls' to list out all adapters.\n");
    printf("Use 'search' to search adapters.\n");
    printf("Use 'update' to  update adapter specifications.\n");
    printf("Use 'clear' to clear console.\n\n");
    printf("Use 'exit' to exit the application.\n\n");
}