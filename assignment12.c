#include <stdio.h>
#include <conio.h>
struct complex
{
   float real, imag;
}a, b, c;
   struct complex read(void);
   void display(struct complex);
   struct complex add(struct complex, struct complex);
   struct complex sub(struct complex, struct complex);
  
void main ()
{
   
   printf("Enter the 1st complex number\n ");
   a = read();
   display(a);
   printf("Enter  the 2nd complex number\n");
   b = read();
   display(b);
   printf("Addition\n ");
   c = add(a, b);
   display(c);
   printf("Substraction\n ");
   c = sub(a, b);
   display(c);
   
   
  
  getch();
}
struct complex read(void)
{
   struct complex t;
   printf("Enter the real part\n");
   scanf("%f", &t.real);
   printf("Enter the imaginary part\n");
   scanf("%f", &t.imag);
   return t;
}
void display(struct complex a)
{
   printf("Complex number  is\n");
   printf(" %.1f + i %.1f", a.real, a.imag);
   printf("\n");
}
struct complex add(struct complex p, struct complex q)
{
   struct complex t;
   t.real = (p.real + q.real);
   t.imag = (p.imag + q.imag);
   return t;
}
struct complex sub(struct complex p, struct complex q)
{
   struct complex t;
   t.real = (p.real - q.real);
   t.imag = (p.imag - q.imag);
   return t;
}
