#include <stdio.h>
#include <ctype.h>

char read();
void grade();

int main(void)
{
    grade();
}

char read()
{
    char gr;

    scanf("%c",&gr);

    gr = toupper(gr);

    return gr;
}

void grade()
{
    char g;
    int a70 = 0, b50 = 0;

    printf("Enter grades of students in CCP (Press X to Execute)\n");

    while (g!='X')
    {
        g = read();

        switch (g)
        {
            case 'S':
            case 'A':
            case 'B':
                a70++;
                break;
            case 'C':
            case 'D':
                break;
            case 'E':
            case 'F':
                b50++;
                break;
        }
    }

    printf("Above 70 = %d\n", a70);
    printf("Below 50 = %d\n", b50);
}
